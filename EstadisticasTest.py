from unittest import TestCase

__author__ = 'Wilson'

import Estadisticas

class EstadisticasTest(TestCase):
    def test_valores_estadisticos(self):
        estadisticas = Estadisticas.Estadisticas().valores_estadisticos("")
        self.assertEqual(estadisticas[0], 0, "Cadena Vacia")
        self.assertEqual(estadisticas[1], 0, "Cadena Vacia")
        self.assertEqual(estadisticas[2], 0, "Cadena Vacia")
        self.assertEqual(estadisticas[3], 0, "Cadena Vacia")

    def test_valores_estadisticosUnNumero(self):
        estadisticas = Estadisticas.Estadisticas().valores_estadisticos("5")
        self.assertEqual(estadisticas[0], 1, "Un numero")
        self.assertEqual(estadisticas[1], 5, "Un numero")
        self.assertEqual(estadisticas[2], 5, "Un numero")
        self.assertEqual(estadisticas[3], 5, "Un numero")

    def test_valores_estadisticosDosNumeros(self):
        estadisticas = Estadisticas.Estadisticas().valores_estadisticos("6,7")
        self.assertEqual(estadisticas[0], 2, "Dos numeros")
        self.assertEqual(estadisticas[1], 6, "Dos numeros")
        self.assertEqual(estadisticas[2], 7, "Dos numeros")
        self.assertEqual(estadisticas[3], 6.5, "Dos numeros")

    def test_valores_estadisticosNNumeros(self):
         estadisticas = Estadisticas.Estadisticas().valores_estadisticos("5,6,4,7")
         self.assertEqual(estadisticas[0], 4, "N numeros")
         self.assertEqual(estadisticas[1], 4, "N numeros")
         self.assertEqual(estadisticas[2], 7, "N numeros")
         self.assertEqual(estadisticas[3], 5.5, "N numeros")

