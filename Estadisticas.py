__author__ = 'Wilson'

class Estadisticas:
    def valores_estadisticos(self, cadena):
        if cadena == "":
             return[0,0,0,0]
        else:
            numeros = cadena.split(",")
            min = int(numeros[0])
            max = int(numeros[0])
            num_elem = len(numeros)
            sum = 0
            if num_elem > 1:
                for num in numeros:
                    if min > int(num):
                        min = int(num)
                    if max < int(num):
                        max = int(num)
                    sum = sum + float(num)
                prom = sum / num_elem
            else:
                prom = int(numeros[0])
            return [num_elem,min,max,prom]
